package ch.ethz.globis.isk.domain;


import javax.validation.constraints.NotNull;

import ch.ethz.globis.isk.util.CheckPages;


/**
 * A standalone publication that is part of a book. It can represent an
 * article, if the book is a collection of articles. Ic can also represent a
 * book chapter.
 */
public interface InCollection extends Publication {

    public String getNote();

    public void setNote(String note);

    @CheckPages
    public String getPages();

    public void setPages(@CheckPages String pages);

    @NotNull
    public Book getParentPublication();

    public void setParentPublication(@NotNull Book book);
}