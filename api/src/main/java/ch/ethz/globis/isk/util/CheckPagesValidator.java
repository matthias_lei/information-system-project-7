package ch.ethz.globis.isk.util;

import java.util.regex.Pattern;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class CheckPagesValidator implements ConstraintValidator<CheckPages, String> {


    public void initialize(CheckPages constraintAnnotation) {
    }

    public boolean isValid(String object, ConstraintValidatorContext constraintContext) {

        if (object == null)
            return true;

        return Pattern.matches ("\\d+|\\d+-\\d+", object);
    }

}