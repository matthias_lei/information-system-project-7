package ch.ethz.globis.isk.domain;

import java.util.Calendar;
import java.util.Set;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import ch.ethz.globis.isk.util.CheckAuthorEditor;

/**
 * Defines the base state for a publication. Is inherited by all specialized
 * types of publications.
 */
@CheckAuthorEditor
public interface Publication extends DomainObject {

	@NotNull
    public String getTitle();

    public void setTitle(@NotNull String title);

    public Set<Person> getAuthors();

    public void setAuthors(Set<Person> authors);

    public Set<Person> getEditors();

    public void setEditors(Set<Person> editors);

    @Min(1901)  @Max(2015)
    public Integer getYear();

    public void setYear(@Min(1901) @Max(2015) Integer year);

    public String getElectronicEdition();

    public void setElectronicEdition(String electronicEdition);
}