package ch.ethz.globis.isk.util;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import ch.ethz.globis.isk.domain.InProceedings;
import ch.ethz.globis.isk.domain.Proceedings;
import ch.ethz.globis.isk.domain.Publication;

public class CheckAuthorEditorValidator implements ConstraintValidator<CheckAuthorEditor, Publication> {


    public void initialize(CheckAuthorEditor constraintAnnotation) {
    }

    public boolean isValid(Publication publication, ConstraintValidatorContext constraintContext) {

    	return (!publication.getAuthors().isEmpty() || !publication.getEditors().isEmpty());
//    	if(publication instanceof Proceedings){
//    		return !publication.getEditors().isEmpty();
//    	}else{
//    		return !publication.getAuthors().isEmpty();
//    	}
    }

}