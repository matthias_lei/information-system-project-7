package ch.ethz.globis.isk.domain;


import javax.validation.constraints.NotNull;

import javax.validation.constraints.Null;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.ConstraintComposition;

import ch.ethz.globis.isk.util.CheckPages;


/**
 * A type of article that was published as part of a conference proceedings.
 */
public interface InProceedings extends Publication {

    public String getNote();

    public void setNote(String note);
    
    @CheckPages
    public String getPages();

    public void setPages(@CheckPages String pages);

    @NotNull
    public Proceedings getProceedings();

    public void setProceedings(@NotNull Proceedings proceedings);
}