package ch.ethz.globis.isk.domain;

import ch.ethz.globis.isk.util.CheckPages;

/**
 * Represents an article that is published in an academic journal.
 */
public interface Article extends Publication {

    public String getCdrom();

    public void setCdrom(String cdrom);

    public JournalEdition getJournalEdition();

    public void setJournalEdition(JournalEdition journalEdition);

    @CheckPages
    public String getPages();

    public void setPages(@CheckPages String pages);
}