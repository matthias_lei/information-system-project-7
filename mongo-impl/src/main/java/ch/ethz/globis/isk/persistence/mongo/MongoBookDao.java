package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.Book;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.mongo.MongoBook;
import ch.ethz.globis.isk.domain.mongo.MongoJournalEdition;
import ch.ethz.globis.isk.domain.mongo.MongoPerson;
import ch.ethz.globis.isk.domain.mongo.MongoPublication;
import ch.ethz.globis.isk.domain.mongo.MongoSeries;
import ch.ethz.globis.isk.persistence.BookDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Repository
public class MongoBookDao extends MongoDao<String, Book> implements BookDao {

    @Override
    public Book findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    protected Class<MongoBook> getStoredClass() {
        return MongoBook.class;
    }

    @Override
    protected String collection() {
        return "publication";
    }

    @Override
    public Book createEntity() {
        return new MongoBook();
    }
    
    @Override
    protected BasicDBObjectBuilder domainInfo(Book entity, DB db){
        ArrayList<DBRef> authorRefs = new ArrayList<>();
        for (Person person : entity.getAuthors()) {
            authorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class), person.getId()));
        }
        ArrayList<DBRef> editorRefs = new ArrayList<>();
        for (Person person : entity.getEditors()) {
            editorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class),person.getId()));
        }
        
        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
        add("title", entity.getTitle(), builder);
        add("electronicEdition", entity.getElectronicEdition(), builder);
        add("year", entity.getYear(), builder);
        add("authors", authorRefs, builder);
        add("editors", editorRefs, builder);
        
        add("cdrom", entity.getCdrom(), builder);
        add("isbn", entity.getIsbn(), builder);
        add("month", entity.getMonth(), builder);
        if(entity.getPublisher() != null){
        	add("publisher", createRef(db, MongoPublication.class, entity.getPublisher().getId()), builder);
        }
        if(entity.getSeries() != null){
        	add("series", createRef(db, MongoSeries.class, entity.getSeries().getId()), builder);
        }
        return builder;
    }
}
