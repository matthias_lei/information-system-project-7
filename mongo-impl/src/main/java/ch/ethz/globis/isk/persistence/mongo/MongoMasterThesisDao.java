package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.MasterThesis;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.PhdThesis;
import ch.ethz.globis.isk.domain.mongo.MongoMasterThesis;
import ch.ethz.globis.isk.domain.mongo.MongoPerson;
import ch.ethz.globis.isk.domain.mongo.MongoPublication;
import ch.ethz.globis.isk.domain.mongo.MongoSchool;
import ch.ethz.globis.isk.persistence.MasterThesisDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Repository
public class MongoMasterThesisDao extends MongoDao<String, MasterThesis> implements MasterThesisDao {

    @Override
    public MasterThesis findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    protected Class<MongoMasterThesis> getStoredClass() {
        return MongoMasterThesis.class;
    }

    @Override
    protected String collection() {
        return "publication";
    }

    @Override
    public MasterThesis createEntity() {
        return new MongoMasterThesis();
    }
    
    @Override
    protected BasicDBObjectBuilder domainInfo(MasterThesis entity, DB db){
        ArrayList<DBRef> authorRefs = new ArrayList<>();
        for (Person person : entity.getAuthors()) {
            authorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class), person.getId()));
        }
        ArrayList<DBRef> editorRefs = new ArrayList<>();
        for (Person person : entity.getEditors()) {
            editorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class),person.getId()));
        }
        
        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
        add("title", entity.getTitle(), builder);
        add("electronicEdition", entity.getElectronicEdition(), builder);
        add("year", entity.getYear(), builder);
        add("authors", authorRefs, builder);
        add("editors", editorRefs, builder);
        
        if(entity.getSchool() != null){
        	add("school", createRef(db, MongoSchool.class, entity.getSchool().getId()), builder);
        }
        return builder;
    }
}
