package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.InCollection;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.mongo.MongoBook;
import ch.ethz.globis.isk.domain.mongo.MongoInCollection;
import ch.ethz.globis.isk.domain.mongo.MongoPerson;
import ch.ethz.globis.isk.domain.mongo.MongoProceedings;
import ch.ethz.globis.isk.persistence.InCollectionDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MongoInCollectionDao extends MongoDao<String, InCollection> implements InCollectionDao {

    @Override
    protected Class<MongoInCollection> getStoredClass() {
        return MongoInCollection.class;
    }

    @Override
    protected String collection() {
        return "publication";
    }

    @Override
    public InCollection createEntity() {
        return new MongoInCollection();
    }

    @Override
    public InCollection findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public List<InCollection> findByBookIdOrderByYear(String bookId) {
        return queryByReferenceIdOrderByYear("parentPublication.$id", bookId);
    }
    
    @Override
    protected BasicDBObjectBuilder domainInfo(InCollection entity, DB db) {
        ArrayList<DBRef> authorRefs = new ArrayList<>();
        for (Person person : entity.getAuthors()) {
            authorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class), person.getId()));
        }
        ArrayList<DBRef> editorRefs = new ArrayList<>();
        for (Person person : entity.getEditors()) {
            editorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class), person.getId()));
        }
        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
        builder.add("title", entity.getTitle());
        builder.add("electronicEdition", entity.getElectronicEdition());
        builder.add("year", entity.getYear());
        builder.add("authors", authorRefs);
        builder.add("editors", editorRefs);

        add("note", entity.getNote(), builder);
        add("pages", entity.getPages(), builder);
        if(entity.getParentPublication() != null){
        	add("proceedings", createRef(db, MongoBook.class, entity.getParentPublication().getId()), builder);
        }
        return builder;
    }
}
