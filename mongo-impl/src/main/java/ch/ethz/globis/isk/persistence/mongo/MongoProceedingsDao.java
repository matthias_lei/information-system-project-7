package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.InProceedings;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Proceedings;
import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.mongo.MongoConferenceEdition;
import ch.ethz.globis.isk.domain.mongo.MongoPerson;
import ch.ethz.globis.isk.domain.mongo.MongoProceedings;
import ch.ethz.globis.isk.domain.mongo.MongoPublication;
import ch.ethz.globis.isk.domain.mongo.MongoPublisher;
import ch.ethz.globis.isk.domain.mongo.MongoSeries;
import ch.ethz.globis.isk.persistence.ProceedingsDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Repository
public class MongoProceedingsDao extends MongoDao<String, Proceedings> implements ProceedingsDao {

    @Override
    protected Class<MongoProceedings> getStoredClass() {
        return MongoProceedings.class;
    }

    @Override
    protected String collection() {
        return "publication";
    }

    @Override
    public Proceedings findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public Proceedings createEntity() {
        return new MongoProceedings();
    }
    
    @Override
    protected BasicDBObjectBuilder domainInfo(Proceedings entity, DB db){
        ArrayList<DBRef> authorRefs = new ArrayList<>();
        for (Person person : entity.getAuthors()) {
            authorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class), person.getId()));
        }
        ArrayList<DBRef> editorRefs = new ArrayList<>();
        for (Person person : entity.getEditors()) {
            editorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class),person.getId()));
        }
        
        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
        add("title", entity.getTitle(), builder);
        add("electronicEdition", entity.getElectronicEdition(), builder);
        add("year", entity.getYear(), builder);
        add("authors", authorRefs, builder);
        add("editors", editorRefs, builder);
        
        add("note", entity.getNote(), builder);
        add("volume", entity.getVolume(), builder);
        add("number", entity.getNumber(), builder);
        add("isbn", entity.getIsbn(), builder);
        if(entity.getPublisher() != null){
        	add("publisher", createRef(db, MongoPublisher.class, entity.getPublisher().getId()), builder);
        }
        if(entity.getSeries() != null){
        	add("series", createRef(db, MongoSeries.class, entity.getSeries().getId()), builder);
        }
        if(entity.getConferenceEdition() != null){
        	add("conferenceEdition", createRef(db, MongoConferenceEdition.class, entity.getConferenceEdition().getId()), builder);
        }
        ArrayList<DBRef> pubRefs = new ArrayList<>();
        for (Publication p : entity.getPublications()){
            pubRefs.add(createRef(db, MongoPublication.class, p.getId()));
        }
        
        return builder;
    }
}
