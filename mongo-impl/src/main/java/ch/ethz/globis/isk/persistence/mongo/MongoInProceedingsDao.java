package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.Book;
import ch.ethz.globis.isk.domain.InProceedings;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Proceedings;
import ch.ethz.globis.isk.domain.mongo.MongoInProceedings;
import ch.ethz.globis.isk.domain.mongo.MongoPerson;
import ch.ethz.globis.isk.domain.mongo.MongoProceedings;
import ch.ethz.globis.isk.persistence.InProceedingsDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MongoInProceedingsDao extends MongoDao<String, InProceedings> implements InProceedingsDao {

    @Override
    public InProceedings findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public List<InProceedings> findByProceedingsIdOrderByYear(String proceedingsId) {
        return queryByReferenceIdOrderByYear("parentPublication.$id", proceedingsId);
    }

    @Override
    protected Class<MongoInProceedings> getStoredClass() {
        return MongoInProceedings.class;
    }

    @Override
    protected String collection() {
        return "publication";
    }

    @Override
    public InProceedings createEntity() {
        return new MongoInProceedings();
    }
    
    @Override
    protected BasicDBObjectBuilder domainInfo(InProceedings entity, DB db){
        ArrayList<DBRef> authorRefs = new ArrayList<>();
        for (Person person : entity.getAuthors()) {
            authorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class), person.getId()));
        }
        ArrayList<DBRef> editorRefs = new ArrayList<>();
        for (Person person : entity.getEditors()) {
            editorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class),person.getId()));
        }
        
        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
        add("title", entity.getTitle(), builder);
        add("electronicEdition", entity.getElectronicEdition(), builder);
        add("year", entity.getYear(), builder);
        add("authors", authorRefs, builder);
        add("editors", editorRefs, builder);
        
        add("note", entity.getNote(), builder);
        add("pages", entity.getPages(), builder);
        if(entity.getProceedings() != null){
        	add("proceedings", createRef(db, MongoProceedings.class, entity.getProceedings().getId()), builder);
        }
        return builder;
    }
}
