package ch.ethz.globis.isk.persistence.mongo;

import ch.ethz.globis.isk.domain.Article;
import ch.ethz.globis.isk.domain.JournalEdition;
import ch.ethz.globis.isk.domain.Person;
import ch.ethz.globis.isk.domain.Publication;
import ch.ethz.globis.isk.domain.mongo.MongoArticle;
import ch.ethz.globis.isk.domain.mongo.MongoJournalEdition;
import ch.ethz.globis.isk.domain.mongo.MongoPerson;
import ch.ethz.globis.isk.domain.mongo.MongoPublication;
import ch.ethz.globis.isk.persistence.ArticleDao;
import ch.ethz.globis.isk.util.Filter;
import ch.ethz.globis.isk.util.Operator;

import org.springframework.stereotype.Repository;

import com.mongodb.BasicDBObject;
import com.mongodb.BasicDBObjectBuilder;
import com.mongodb.DB;
import com.mongodb.DBObject;
import com.mongodb.DBRef;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
public class MongoArticleDao extends MongoDao<String, Article> implements ArticleDao {

    @Override
    protected Class<MongoArticle> getStoredClass() {
        return MongoArticle.class;
    }

    @Override
    protected String collection() {
        return "publication";
    }

    @Override
    public Article createEntity() {
        return new MongoArticle();
    }

    @Override
    public Article findOneByTitle(String title) {
        Map<String, Filter> filterMap = new HashMap<>();
        filterMap.put("title", new Filter(Operator.EQUAL, title));
        return findOneByFilter(filterMap);
    }

    @Override
    public List<Article> findByJournalEditionOrderedByYear(String journalEditionId) {
        return queryByReferenceIdOrderByYear("journalEdition.$id", journalEditionId);
    }

    @Override
    protected BasicDBObjectBuilder domainInfo(Article entity, DB db) {
        ArrayList<DBRef> authorRefs = new ArrayList<>();
        for (Person person : entity.getAuthors()) {
            authorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class), person.getId()));
        }
        ArrayList<DBRef> editorRefs = new ArrayList<>();
        for (Person person : entity.getEditors()) {
            editorRefs.add(new DBRef(db, collectionMap.get(MongoPerson.class),person.getId()));
        }
        
        BasicDBObjectBuilder builder = BasicDBObjectBuilder.start();
        add("title", entity.getTitle(), builder);
        add("electronicEdition", entity.getElectronicEdition(), builder);
        add("year", entity.getYear(), builder);
        add("cdrom", entity.getCdrom(), builder);
        add("authors", authorRefs, builder);
        add("editors", editorRefs, builder);
        if(entity.getJournalEdition() != null){
        	builder.add("journalEdition", new DBRef(db, collectionMap.get(MongoJournalEdition.class), entity.getJournalEdition().getId()));
        }
        return builder;
    }
}
